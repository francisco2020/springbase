package com.frannet.database.domain;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface ProductRepository extends CrudRepository<Product,Long> {
	  
	  // Fetch Products by brand
	  List<Product> findByBrand(@Param("brand") String brand);

	  // Fetch Products by color
	  List<Product> findByColor(@Param("color") String color);

	  // Fetch Products by year
	// List<Product> findByYear(int year);
	  
	  // Fetch Products by brand and model
	// List<Product> findByBrandAndModel(String brand, String model);

	  // Fetch Products by brand or color
	// List<Product> findByBrandOrColor(String brand, String color);
	  
	  // Fetch Products by brand and sort by year
	//  List<Product> findByBrandOrderByYearAsc(String brand);
	  
	  // Fetch Products by brand using SQL
	//  @Query("select c from Product c where c.brand = ?1")
	// List<Product> findByBrandSQL(String brand);
	  
	  // Fetch Products by brand using SQL Like
	// @Query("select c from Product c where c.brand like %?1")
	//  List<Product> findByBrandEndsWith(String brand);
	
}
