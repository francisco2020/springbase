package com.frannet.database.domain;

import org.springframework.data.repository.CrudRepository;

public interface ProductOwnerRepository extends CrudRepository<Product_Owner, Long> {

}
