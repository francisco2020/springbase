package com.frannet.database;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;

import com.frannet.database.domain.Owner;
import com.frannet.database.domain.OwnerRepository;
import com.frannet.database.domain.Product;
import com.frannet.database.domain.ProductOwnerRepository;
import com.frannet.database.domain.ProductRepository;
import com.frannet.database.domain.Product_Owner;
import com.frannet.database.domain.User;
import com.frannet.database.domain.UserRepository;

@SpringBootApplication
public class DatabaseApplication {
  
  @Autowired 
  private ProductRepository productRepository;
  
  @Autowired 
  private OwnerRepository ownerRepository;
  
  @Autowired 
  private ProductOwnerRepository productOwnerRepository;
  
  @Autowired 
  private UserRepository urepository;
  
  private static final Logger logger = LoggerFactory.getLogger(DatabaseApplication.class);
  
  public static void main(String[] args) {
    SpringApplication.run(DatabaseApplication.class, args);
    logger.info("Logger Test Spring Boot");
  }
  
  
  @Bean
  CommandLineRunner runner(){
      return args -> {
          Owner ownerFirst = new Owner("Michael" , "Johnson");
          Owner ownerSecond = new Owner("Ana" , "Salazar");
          
          
          Product productFirst = new Product("Ford", "Mustang", "Red",
                  "ADF-1121", 2017, 59000);
          Product productSecond = new Product("Nissan", "Leaf", "White",
                  "SSJ-3002", 2014, 29000);
          Product productThird = new Product("Toyota", "Prius", "Silver",
                  "KKO-0212", 2018, 39000);
          
          // Save Owners
          ownerRepository.save(ownerFirst);
          ownerRepository.save(ownerSecond);
          
          // Save Products
          productRepository.save(productFirst);
          productRepository.save(productSecond);
          productRepository.save(productThird);
          
          Product_Owner productOwnerFirst = new Product_Owner(ownerFirst, productFirst);
          Product_Owner productOwnerSecond = new Product_Owner(ownerFirst, productSecond);
          Product_Owner productOwnerThird = new Product_Owner(ownerSecond, productFirst);
          Product_Owner productOwnerFour = new Product_Owner(ownerSecond, productThird);
          
          // Save Middle table
          productOwnerRepository.save(productOwnerFirst);
          productOwnerRepository.save(productOwnerSecond);
          productOwnerRepository.save(productOwnerThird);
          productOwnerRepository.save(productOwnerFour);
          
          urepository.save(new User("user",
         "$2a$04$1.YhMIgNX/8TkCKGFUONWO1waedKhQ5KrnB30fl0Q01QKqmzLf.Zi",
          "USER"));

          urepository.save(new User("admin",
          "$2a$04$KNLUwOWHVQZVpXyMBNc7JOzbLiBjb9Tk9bP7KNcPI12ICuvzXQQKG", 
          "ADMIN"));
          
        };
     }
  
}