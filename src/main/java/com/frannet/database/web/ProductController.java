package com.frannet.database.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.frannet.database.domain.Product;
import com.frannet.database.domain.ProductRepository;

@RestController
@RequestMapping("/products")
public class ProductController {

    @Autowired
    private ProductRepository repository;
    
    public Iterable<Product> getProducts() {
    	return repository.findAll();
    } 
}
