package com.frannet.database;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.frannet.database.domain.Product;
import com.frannet.database.domain.ProductRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ProductRepositoryTest {

  @Autowired
  private TestEntityManager entityManager;
  
  @Autowired
  private ProductRepository repository;
  
  @Test
  public void saveCar() {
    Product productFirst = new Product("Tesla", "Model X", "White", "ABC-1234",
        2017, 86000);
    entityManager.persistAndFlush(productFirst);
    
    assertThat(productFirst.getId()).isNotNull();
  }
  
  @Test
  public void deleteCars() {
    entityManager.persistAndFlush(new Product("Tesla", "Model X", "White",
        "ABC-1234", 2017, 86000));
    entityManager.persistAndFlush(new Product("Mini", "Cooper", "Yellow",
        "BWS-3007", 2015, 24500));
    
    repository.deleteAll();
    assertThat(repository.findAll()).isEmpty();
  }
  
}