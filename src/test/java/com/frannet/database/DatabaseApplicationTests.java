package com.frannet.database;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.frannet.database.web.ProductController;

@RunWith(SpringRunner.class)
@SpringBootTest
class DatabaseApplicationTests {

	  @Autowired
	  private ProductController controller;

	  @Test
	  public void contextLoads() {
	    assertThat(controller).isNotNull();
	  }
}
